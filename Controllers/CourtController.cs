﻿using System.Globalization;
using AngleSharp;
using Microsoft.AspNetCore.Mvc;

namespace ToernooiChecker.Controllers;

[ApiController]
public class CourtController : BaseController
{

    // private static object Ret;
    // private static object Res;
    private const string DefaultTournament = "f90c9be8-3a44-4354-9df7-dec17d77d6e5";
    private static string TournamentId { get; set; } = DefaultTournament;
    private static string? TournamentDate { get; set; }

    [HttpGet]
    [Route("/setTournament/{tournamentId}")]
    [Route("/setTournament")]
    public object SetTournament(string? tournamentId)
    {
        TournamentId = tournamentId ?? DefaultTournament;
#if DEBUG
        return "OK";
#else
        return Redirect("/wedstrijden");
#endif
    }
    [HttpGet]
    [Route("/setTournamentDate/{tournamentDate}")]
    [Route("/setTournamentDate")]
    public object SetTournamentDate(string? tournamentDate)
    {
        TournamentDate = tournamentDate?.Replace("-", "").Trim();
#if DEBUG
        return "OK";
#else
        return Redirect("/wedstrijden");
#endif
    }
    
    [HttpGet]
    [Route("/court/check")]
    public async Task<object> Check()
    {
        await GetCookie();

        var datePart = string.IsNullOrEmpty(TournamentDate) ? string.Empty : $"/{TournamentDate}";
        var res = await Client.GetAsync(
            $"https://mijnknltb.toernooi.nl/tournament/{TournamentId}/Matches{datePart}");
        var htmlContent = await res.Content.ReadAsStringAsync();
        // var htmlContent = await System.IO.File.ReadAllTextAsync("C:\\projects\\prive\\toernooi-checker\\ToernooiChecker\\toernooi.html");
        
        var config = Configuration.Default.WithDefaultLoader();
        var context = BrowsingContext.New(config);
        var document = await context.OpenAsync(req => req.Content(htmlContent));

        var tournamentName = document.QuerySelector(".hgroup__heading.truncate")?.TextContent;
        Response.Headers.TryAdd("x-tournament", tournamentName);
        Response.Headers.TryAdd("x-tournament-date", TournamentDate);
        var stockElements = document.QuerySelectorAll(".match-group__wrapper .match-group__item"); // Replace with the actual CSS selector for the stock element
        var ret = stockElements.Select(e =>
        {
            var time = e.Parent.ParentElement.QuerySelector("h5.is-sticky").TextContent.Replace("\n", "").Trim();
            
            var header = e.QuerySelectorAll(".match__header-title .nav-link__value");
            var category = header.FirstOrDefault()?.TextContent;
            var round = header.LastOrDefault()?.TextContent;
            var court = e.QuerySelector(".match__header .has-tooltip--wide")?.GetAttribute("title")?.Split(" - ").Last();
            var busy = "Nu bezig".Equals(court);
            if (busy)
            {
                court = e.QuerySelector(".match__header .has-tooltip--wide").ParentElement.Children.Last()?.GetAttribute("title").Split(" - ").Last();
            }

            var body = e.QuerySelector(".match__row-wrapper");
            var score = e.QuerySelectorAll(".match__result .points").Select(set =>
            {
                return set.QuerySelectorAll(".points__cell").Select(setPart => new
                {
                    games = int.Parse(setPart.TextContent.Trim()),
                    won = setPart.ClassName!.Contains("points__cell--won")
                }).ToList();
            }).ToList();
            var parties = body.QuerySelectorAll(".match__row");
            
            var partyOne = parties.First();
            var partyOneWinner = partyOne.ClassName!.Contains("has-won");
            var playersPartyOneElements = partyOne!.QuerySelectorAll(".match__row-title-value");
            var playersPartyOne = playersPartyOneElements.Select(x => x.QuerySelector(".nav-link__value").TextContent);
            
            var partyTwo = parties.Last();
            var partyTwoWinner = partyTwo.ClassName!.Contains("has-won");
            var playersPartyTwoElements = partyTwo.QuerySelectorAll(".match__row-title-value");
            var playersPartyTwo = playersPartyTwoElements.Select(x => x.QuerySelector(".nav-link__value").TextContent);

            var specialReason = body.QuerySelector(".tag--warning")?.TextContent;

            var date = string.IsNullOrEmpty(TournamentDate) ? DateTime.Now : DateTime.ParseExact(TournamentDate, "yyyyMMdd", CultureInfo.InvariantCulture);
            var timeSplit = time.Split(":").Select(e => Convert.ToInt32(e)).ToArray();
            var dateTime = new DateTime(date.Year, date.Month, date.Day, timeSplit[0], timeSplit[1], 0);
            
            return new
            {
                score,
                dateTime,
                category,
                round,
                court,
                playersPartyOne,
                partyOneWinner,
                playersPartyTwo,
                partyTwoWinner,
                specialReason,
                busy
            };
        });
        // Ret = ret;
        return ret;
    }
}