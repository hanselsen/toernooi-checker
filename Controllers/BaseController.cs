﻿using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.AspNetCore.Mvc;

namespace ToernooiChecker.Controllers;

public class BaseController : ControllerBase
{
    private static CookieContainer? CookieContainer { get; set; }
    private static HttpClientHandler? HttpClientHandler { get; set; }
    protected static HttpClient? Client { get; private set; }

    public BaseController()
    {
        if (Client != null) return; 
        CookieContainer = new CookieContainer();
        HttpClientHandler = new HttpClientHandler();
        HttpClientHandler.CookieContainer = CookieContainer;
        Client = new HttpClient(HttpClientHandler);
    }
    
    protected async Task GetCookie()
    {
        await Client.GetAsync("https://mijnknltb.toernooi.nl/cookiewall/");
        var request = new HttpRequestMessage(HttpMethod.Post, "https://mijnknltb.toernooi.nl/cookiewall/Save");
        var collection = new List<KeyValuePair<string, string>>
        {
            new("ReturnUrl", "/"),
            new("SettingsOpen", "false"),
            new("CookiePurposes", "2"),
            new("CookiePurposes", "4"),
            new("CookiePurposes", "8"),
            new("CookiePurposes", "16")
        };
        var content = new FormUrlEncodedContent(collection);
        request.Content = content;
        await Client.SendAsync(request);
    }
}