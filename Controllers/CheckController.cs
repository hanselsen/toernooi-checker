﻿using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.AspNetCore.Mvc;

namespace ToernooiChecker.Controllers;

[ApiController]
public class CheckController : BaseController
{
    [HttpGet]
    [Route("/check")]
    public async Task<object> Check([FromQuery] string date, [FromQuery] string zipcode, [FromQuery] string distance)
    {
        await GetCookie();
        var tournaments = await GetTournaments(date, date, zipcode, distance);
        var players = new Dictionary<string, List<string>>();
        foreach (var (id, name) in tournaments)
        {
            var p = await GetPlayers(id);
            foreach (var playerName in p)
            {
                if(!players.ContainsKey(playerName))
                    players.Add(playerName, new List<string>());
                players[playerName].Add(id);
            }
        }
        return players
            .Where(p => p.Value.Count > 1)
            .ToDictionary(p => p.Key, p => p.Value.ToDictionary(x => tournaments[x], x => x));
    }
    [HttpGet]
    [Route("/check-registrations")]
    public async Task<object> GetRegistrations([FromQuery] string date, [FromQuery] string zipcode, [FromQuery] string distance, [FromQuery] string playerName)
    {
        await GetCookie();
        var tournaments = await GetTournaments(date, date, zipcode, distance);
        var registrations = new Dictionary<string, List<string>>();
        foreach (var (id, name) in tournaments)
        {
            var players = await GetPlayers(id);
            var player = players.FirstOrDefault(pp => pp.Trim().Equals(playerName, StringComparison.InvariantCultureIgnoreCase) || 
                                                      string.Join(" ", pp.Split(",").Reverse()).Trim().Contains(playerName, StringComparison.InvariantCultureIgnoreCase));
            if(player == null) continue;
            if (!registrations.ContainsKey(player))
            {
                registrations[player] = new List<string>();
            }
            registrations[player].Add(id);
        }
        return registrations
            .ToDictionary(p => p.Key, p => p.Value.ToDictionary(x => tournaments[x], x => x));
    }

    private async Task<List<string>> GetPlayers(string id)
    {
        var request = new HttpRequestMessage(HttpMethod.Post,
            $"https://mijnknltb.toernooi.nl/tournament/{id}/Players/GetPlayersContent");
        request.Content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
        {
            new("X-Requested-With", "XMLHttpRequest")
        });
        request.Headers.Add("origin", "https://mijnknltb.toernooi.nl");
        var response = await Client.SendAsync(request);
        var responseContent = await response.Content.ReadAsStringAsync();
        var pattern = @"player.aspx\?id=([0-9|a-z|-]*).*?link__value"">(.*?)<";
        var matches = Regex.Matches(responseContent, pattern, RegexOptions.Singleline);
        return matches.Select(m => m.Groups[2].Value).ToHashSet().ToList();
    }

    private async Task<Dictionary<string, string>> GetTournaments(string start, string end, string zipcode, string distance)
    {
        var tournaments = new Dictionary<string, string>();
        var request = new HttpRequestMessage(HttpMethod.Post, "https://mijnknltb.toernooi.nl/find/tournament/DoSearch");
        var content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
        {
            new("Page", "1000"),
            new("TournamentFilter.StartDate", start),
            new("TournamentFilter.EndDate", end),
            new("TournamentFilter.PostalCode", zipcode),
            new("TournamentFilter.Distance", distance),
            new("TournamentExtendedFilter.StatusFilterID", "false"),
            new("X-Requested-With", "XMLHttpRequest")
        });
        request.Content = content;
        var response = await Client.SendAsync(request);
        var responseContent = await response.Content.ReadAsStringAsync();
        const string pattern = @"<a href=""(\S*?)"" title=""(.*?)"" class=""media__link"" >";
        var matches = Regex.Matches(responseContent, pattern, RegexOptions.Singleline);
        foreach (Match match in matches)
        {
            tournaments.TryAdd(match.Groups[1].Value.Split("=").Last(), HttpUtility.HtmlDecode(match.Groups[2].Value));
        }

        return tournaments;
    }
}