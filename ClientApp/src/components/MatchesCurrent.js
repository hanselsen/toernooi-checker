import React from 'react';
import './matches.css';
import Court from "./Court";
 import {List} from "immutable";
import dayjs from "dayjs";

const MatchesOverview = props => {
    const [matches, setMatches] = React.useState([]);
    const [scrolling, setScrolling] = React.useState(false);
    const [tournamentName, setTournamentName] = React.useState('');
    const [tournamentDate, setTournamentDate] = React.useState('');
    React.useEffect(() => {
        if(matches?.length) return;
        const getMatches = async () => {
            const results = await fetch('/court/check');
            if(results.headers.get('X-tournament')) setTournamentName(results.headers.get('X-tournament'));
            if(results.headers.get('X-tournament-date')) setTournamentDate(results.headers.get('X-tournament-date'));
            const m = await results.json();
            setMatches(m);
            setTimeout(() => {
                const completedMatches = List(m)
                    .filter(m => m.partyOneWinner || m.partyTwoWinner)
                    .filter(m => dayjs(m.dateTime) < dayjs())
                    .filter(m => !m.specialReason);
                if(completedMatches.size) {
                    window.location.href = '/wedstrijden-gespeeld';
                } else {
                    window.location.reload();
                }
            }, 59 * 1000);
        };
        getMatches().then();
        setTimeout(() => setScrolling(true), 100);
    }, []);
    
    return (
        <div>
            <div className="title">
                <div className={`progress long ${scrolling && 'scrolling'}`}/>
                <div style={{fontWeight: 'bold', flex: 1}}>Huidige wedstrijden</div>
                <div>{tournamentName} {!!tournamentDate && `- ${tournamentDate}`}</div>
            </div>
            <div className="park-container">
                <div className="park">
                    <div className="courts">
                        <div className="path horizontal">
                            <img
                                style={{
                                    position: "absolute",
                                    zIndex: 100000,
                                    left: 0,
                                    top: -160,
                                }}
                                alt="uiltje3"
                                src="https://www.uiltjebrewing.com/app/themes/nfc-uiltjebrewing/assets/images/happy-owl.png"
                            />
                        </div>
                        <Court index={6} matches={matches}/>
                        <Court index={5} matches={matches}/>
                        <div className="path horizontal"></div>
                        <Court index={4} matches={matches}/>
                        <Court index={3} matches={matches}/>
                        <div className="path horizontal"></div>
                        <Court index={2} matches={matches}/>
                        <Court index={1} matches={matches}/>
                        <img
                            style={{marginTop: 20}}
                            src="/paper-planet-logo.svg"
                            alt="paper planet"/>
                    </div>
                    <div className="path vertical"></div>
                    <div className="courts two">
                        <div className="path horizontal right"></div>
                        <Court index={10} matches={matches}/>
                        <Court index={9} matches={matches}/>
                        <div className="path horizontal right"></div>
                        <Court index={8} matches={matches}/>
                        <Court index={7} matches={matches}/>
                        <div className="path horizontal right"></div>
                        <div className="padel-courts">
                            <div className="coming-soon">
                                Coming soon
                                <img
                                    className="uiltje"
                                    style={{
                                        zIndex: 100000,
                                        position: "absolute",
                                        scale: '30%',
                                        left: '-50%',
                                        marginTop: 20
                                    }}
                                    alt="uiltje2"
                                    src="https://www.uiltjebrewing.com/app/webpc-passthru.php?src=https://www.uiltjebrewing.com/app/uploads/2021/02/fancy_pants-1-360x311.png"/>
                            </div>
                            <Court padel index={1} matches={[/*ALS PADEL KLAAR IS*/]}/>
                            <Court padel index={2} matches={[/*ALS PADEL KLAAR IS*/]}/>
                            <Court padel index={3} matches={[/*ALS PADEL KLAAR IS*/]}/>
                            <Court padel index={4} matches={[/*ALS PADEL KLAAR IS*/]}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default MatchesOverview;