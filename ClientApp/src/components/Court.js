import dayjs from 'dayjs';

const Court = props => {
    const {
        index,
        matches,
        padel
    } = props;
    const match = matches
        .filter(m => m.court.toLowerCase() === `baan ${index}`.toLowerCase())
        .filter(m => m.category.indexOf('Padel') === -1 || padel)
        .find(m => m.busy);

    return (
        <div className={`court ${padel && 'padel'} ${!match && 'empty'}`}>
            {!!match && (
                <>
                    <div className="category">{match?.category}</div>
                    <div className="time">{dayjs(match.dateTime).format('HH:mm')}</div>
                </>
            )}
            <div className={`index ${index > 9 && 'large'}`}>{index}</div>
            <div className="team one">
                {match?.playersPartyOne.map(((player, i) => (
                    <div key={i} className={`player player${i}`}>{player}</div>
                )))}
            </div>
            <div className="team two">
                {match?.playersPartyTwo.map(((player, i) => (
                    <div key={i} className={`player player${i}`}>{player}</div>
                )))}
            </div>
        </div>
    );
};

export default Court;