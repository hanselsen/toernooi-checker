  import React from 'react';
import {Button, Input} from "reactstrap";

export const Home = () => {
    const [date, setDate] = React.useState(`${new Date().getFullYear()}-07-19`);
    const [zip, setZip] = React.useState('5662AA');
    const [distance, setDistance] = React.useState('30');
    const [player, setPlayer] = React.useState('Hans van den Elsen');
    const [loading, setLoading] = React.useState(false);
    const [result, setResult] = React.useState({});
    const [filter, setFilter] = React.useState('');
    
    return (
        <div style={{
            display: 'flex',
            flexDirection: 'column',
            height: '100vh'
        }}>
            <h3>Inschrijvingen in de regio</h3>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                gap: 10
            }}>
                <label>
                    <span>Datum</span>
                    <Input
                        disabled={loading}
                        type="text"
                        placeholder="YYYY-MM-DD"
                        value={date}
                        onChange={e => setDate(e.target.value)}
                    />
                </label>
                <label>
                    <span>Postcode</span>
                    <Input
                        disabled={loading}
                        type="text"
                        placeholder="1234AB"
                        value={zip}
                        onChange={e => setZip(e.target.value)}
                    />
                </label>
                <label>
                    <span>Afstand (km)</span>
                    <Input
                        disabled={loading}
                        type="number"
                        placeholder="30"
                        value={distance}
                        onChange={e => setDistance(e.target.value)}
                    />
                </label>
                <Button
                    disabled={loading}
                    onClick={() => {
                        setResult({});
                        setLoading(true);
                        fetch(`/check?distance=${distance}&date=${date}&zipcode=${zip}`).then(async res => {
                            setLoading(false);
                            setResult(await res.json());
                        });
                    }}
                    color="primary"
                >Zoek naar dubbele inschrijvingen</Button>
                <label>
                    <span>Naam speler</span>
                    <Input
                        disabled={loading}
                        type="text"
                        value={player}
                        onChange={e => setPlayer(e.target.value)}
                    />
                </label>
                <Button
                    disabled={loading}
                    onClick={() => {
                        setResult({});
                        setLoading(true);
                        fetch(`/check-registrations?distance=${distance}&date=${date}&zipcode=${zip}&playerName=${player}`).then(async res => {
                            setLoading(false);
                            setResult(await res.json());
                        });
                    }}
                    color="primary"
                >Zoek naar inschrijvingen</Button>
            </div>
            <br/>
            <br/>
            <Input
                disabled={loading}
                type="text"
                value={filter}
                placeholder="Toernooifilter"
                onChange={e => setFilter(e.target.value)}
            />

            <div style={{flex: 1, overflow: 'auto'}}>
                {Object.keys(result).map(player => {
                    if(filter && Object.keys(result[player]).join('').toLowerCase().indexOf(filter.toLowerCase()) === -1) {
                        return null;
                    }
                    return (
                        <div key={player}>
                            <div>{player}</div>
                            <ul style={{
                                display: 'flex',
                                flexDirection: 'column',
                                marginLeft: 20
                            }}>{Object.keys(result[player]).map(tournamentName => (
                                <li key={tournamentName}>
                                    <a href={`https://mijnknltb.toernooi.nl/tournament/${result[player][tournamentName]}/players`}>{tournamentName}</a>
                                </li>
                            ))}</ul>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};