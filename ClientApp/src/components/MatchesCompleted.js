import React from 'react';
import './matches.css';
import {List} from 'immutable';
import dayjs from "dayjs";
import {Box, Grid} from "@mui/material";

require('./matchesCompleted.css');
require('./title.css');

const MatchesCompleted = props => {
    const [matches, setMatches] = React.useState([]);
    const [scrolling, setScrolling] = React.useState(false);
    const [skip, setSkip] = React.useState(0);
    const [tournamentName, setTournamentName] = React.useState('');
    const [tournamentDate, setTournamentDate] = React.useState('');
    React.useEffect(() => {
        if(matches?.length) return;
        const getMatches = async () => {
            const results = await fetch('/court/check');
            if(results.headers.get('X-tournament')) setTournamentName(results.headers.get('X-tournament'));
            if(results.headers.get('X-tournament-date')) setTournamentDate(results.headers.get('X-tournament-date'));
             const m = await results.json();
            setMatches(m);
            const completed = m
                .filter(m2 => m2.partyOneWinner || m2.partyTwoWinner)
                .filter(m2 => dayjs(m2.dateTime) < dayjs())
                .filter(m2 => !m2.specialReason);
            const long = completed.length > 8;
            setTimeout(() => {
                window.location.href = '/wedstrijden';
            }, (long ? 31 : 16) * 1000 );
            if(long) {
                setTimeout(() => {
                    setSkip(8);
                }, 15 * 1000);
            }
            setTimeout(() => setScrolling(true), 100);
        };
        getMatches().then();
    }, []);
    
    const completedMatches = List(matches)
        .filter(m => m.partyOneWinner || m.partyTwoWinner)
        .filter(m => dayjs(m.dateTime) < dayjs())
        .reverse()
        .skip(skip)
        .take(8)
        .reverse()
        .toJS();
    
    return (
        <Box className={`wrapper`}>
            <div className="background" />
            <div className="title">
                <div className={`progress ${completedMatches.length > 8 ? 'short' : 'superShort'} ${scrolling && 'scrolling'}`}/>
                <div style={{fontWeight: 'bold', flex: 1}}>Gespeelde wedstrijden</div>
                <div>{tournamentName} {!!tournamentDate && `- ${tournamentDate}`}</div>
            </div>
            <Grid container spacing={6} sx={{marginTop: 1, marginBottom: 10}}>
            {completedMatches.map(m => (
                    <Grid item xs={12} sm={12} md={6} key={`${m.dateTime}-${m.court}-${m.category}`}>
                        <Box className="match">
                            <Box className="info">
                                <Box sx={{flex: 1}}>{dayjs(m.dateTime).format('HH:mm')}</Box>
                                <Box sx={{fontSize: 12}}>{m.category} - {m.round}</Box>
                            </Box>
                            <Box className="players">
                                <Box className={`party ${m.partyOneWinner && 'winner'}`}>
                                    {m.playersPartyOne.map(playerName => <span key={playerName}>{playerName}</span>)}
                                </Box>
                                <div className="separator" />
                                <Box className={`party ${m.partyTwoWinner && 'winner'}`}>
                                    {m.playersPartyTwo.map(playerName => <div key={playerName}>{playerName}</div>)}
                                </Box>
                            </Box>
                            <Box className="outcome">
                                {!!m.score.length && (
                                    <Box className="score">
                                        {m.score.map((set, s) => (
                                            <Box className="set" key={s}>
                                                <span className={set[0].won && 'won'}>{set[0].games}</span>
                                                <span>-</span>
                                                <span className={set[1].won && 'won'}>{set[1].games}</span>
                                            </Box>
                                        ))}
                                    </Box>
                                )}
                                {m.specialReason && (
                                    <Box className="specialReason">{m.specialReason}</Box>
                                )}
                            </Box>
                        </Box>
                    </Grid>
                ))}
            </Grid>
        </Box>
    );

}

export default MatchesCompleted;