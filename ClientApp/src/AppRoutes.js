import { Home } from "./components/Home";
import MatchesCurrent from "./components/MatchesCurrent";
import MatchesCompleted from "./components/MatchesCompleted";

const AppRoutes = [
  {
    index: true,
    element: <Home />
  },
  {
    path: '/wedstrijden',
    element: <MatchesCurrent />
  },
  {
    path: '/wedstrijden-gespeeld',
    element: <MatchesCompleted />
  }
];

export default AppRoutes;
